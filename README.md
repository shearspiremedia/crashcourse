# Crashcourse

This project contains code examples from `https://coursetro.com/posts/code/174/Angular-8-Tutorial-&-Crash-Course`

Topics include: routing, component styling, and retrieving data from an api. This project uses SCSS.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

## Helpful Notes

 Summary for new workspace in Angular

  ng new somename  
  cd somename  
  ng add @briebug/jest-schematic  
  ng g @briebug/jest-schematic:add  
  
  run npm install if you are cloning an existing repo
  
  If linking an existing local repo to a new empty bitbucket repo, copy the git remote code like:
  
  git remote add origin https://shearspiremedia@bitbucket.org/shearspiremedia/crashcourse.git
  
  Then run an initial commit of the files in the local repo before pushing everything to the remote repo with
  
  git push -u origin master
  
  NOTE: To prepare a dist folder that will work correctly when installed in a subdirectory of a server:
  
  modify package.json to include homepage: "."
  
  "name": "crashcourse",  
  "version": "0.0.0",  
  "homepage": ".",  
  "scripts": {  
    "ng": "ng",  
    "start": "ng serve",  
    "build": "ng build",  
    "test": "jest",  
    "lint": "ng lint",  
    "e2e": "ng e2e",  
    "test:watch": "jest --watch"  
  },
  
  modify angular.json to include the redirects file in the list of assets.  
  also, you may wish to change the output path to eliminate the subdirectory and just post to dist  
  Look for projects.architect.options.assets
  
  "options": {  
            "outputPath": "dist",  
            "index": "src/index.html",  
            "main": "src/main.ts",  
            "polyfills": "src/polyfills.ts",  
            "tsConfig": "tsconfig.app.json",  
            "aot": false,  
            "assets": [  
              "src/favicon.ico",  
              "src/assets",  
              "src/_redirects"  
            ],  

Create the _redirects file in the src directory. Type only the following into the file.  
/*    /index.html   200

Also change the base href in the index.html

<base href=".">




  